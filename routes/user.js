const path = require('path');

const express = require('express');
const router = express.Router();

const dirName = require('../helper/path');

router.get('/',(req, res, next) =>{
    res.sendFile(path.join(dirName, 'views', 'user.html'));
});

module.exports = router;